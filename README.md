# RPA GUIDE - UiPath

This guide is to share what I'm learned of Robotic Process Automation (RPA).

I used the Community version of [UiPath][uipath_url], and I'll make it in Spanish and English. For now, I only have the Spanish version.

Enjoy it!

-----

Esta guía es para compartir lo que he aprendido de Automatización Robótica de Procesos (RPA).

Utilicé la versión Community de [UiPath][uipath_url], y la haré en Español e Inglés. Por ahora, sólo tengo la versión en Español.

¡Disfrútala!

[uipath_url]: https://www.uipath.com
